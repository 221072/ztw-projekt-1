﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {ReactiveFormsModule}    from '@angular/forms';
import { HttpModule } from '@angular/http';

// used to create fake backend
import { fakeBackendProvider } from './_helpers/index';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AdminAuthGuard } from './_guards/index';
import { AuthenticationService, UserService } from './_services/index';
import { LoginComponent } from './login/index';
import { AdminHomeComponent } from './adminHome/index';
import {AccountManagementComponent} from "./accountManagement/accountManagement.component";
import {AccountFormComponent} from "./accountForm/accountForm.component";
import {UserHomeComponent} from "./userHome/userHome.component";
import {UserAuthGuard} from "./_guards/user.auth.guard";
import {TaskService} from "./_services/task.service";
import {TaskRepository} from "./_repositories/taskRepository";
import {CategoryRepository} from "./_repositories/categoryRepository";
import {CategoryService} from "./_services/category.service";
import {CategoryManagementComponent} from "./categoryManagement/categoryManagement.component";
import {AddTaskComponent} from "./addTask/addTask.component";
import {EditTaskComponent} from "./editTask/editTask.component";
import {UserRepository} from "./_repositories/userRepository";
import {EditCategoryComponent} from "./editCategory/editCategory.component";
import {AddCategoryComponent} from "./addCategory/addCategory.component";
import {HomeComponent} from "./home/home.component";

@NgModule({
    imports: [
        ReactiveFormsModule,
        BrowserModule,
        HttpModule,
        routing
    ],
    declarations: [
        AppComponent,
        LoginComponent,

        AdminHomeComponent,
        UserHomeComponent,

        CategoryManagementComponent,
        EditCategoryComponent,
        AddCategoryComponent,

        AccountFormComponent,
        AccountManagementComponent,

        AddTaskComponent,
        EditTaskComponent,


        HomeComponent
    ],
    providers: [
        AdminAuthGuard,
        UserAuthGuard,

        TaskRepository,
        CategoryRepository,
        UserRepository,

        AuthenticationService,
        UserService,
        TaskService,
        CategoryService,

        // providers used to create fake backend
        fakeBackendProvider,
        MockBackend,
        BaseRequestOptions
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }