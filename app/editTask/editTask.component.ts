import {Component, OnInit} from '@angular/core';

import {UserService} from '../_services/index';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FormNature} from "../_helpers/formNature";
import {AuthenticationService} from "../_services/authentication.service";
import {TaskStatus} from "../_models/taskStatus";
import {TaskService} from "../_services/task.service";
import {Task} from "../_models/task";
import {convertDateToDateTimeLocalControlValue} from "../_helpers/dateTimeConverter";
import {maxValue, minValue} from "../_customValidators/minValue";
import {TaskBoardDate} from "../_models/taskBoardDate";
import {Category} from "../_models/category";
import {CategoryService} from "../_services/category.service";

@Component({
    moduleId: module.id,
    templateUrl: 'editTask.component.html'
})
export class EditTaskComponent implements OnInit {
    task: FormGroup;
    taskObject: Task;

    fail:boolean;

    categories:Category[];

    statusEnum=TaskStatus;
    statuses:string[];

    constructor(
        private router:Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private taskService: TaskService,
        private authService: AuthenticationService,
        private categoryService: CategoryService
    ) {
    }

    ngOnInit() {
        this.statuses=TaskStatus.toStringArray();

        this.categoryService.getCategories()
            .subscribe(response => {
                console.log('ALL CATEGORIES');
                this.categories=response;
                console.log(JSON.stringify(response));
            });

        this.task=this.formBuilder.group({
            name: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
            categoryId: ['',[Validators.required]],
            startDate: ['', []],
            duration: ['', [minValue(0),maxValue(20)]],
            status: [TaskStatus.fromEnumToString(TaskStatus.New),[Validators.required]],
            description: ['',[]],
            webNotificationDate: ['', []],
            eMailNotificationDate: ['', []]
        });

        this.route.params.subscribe(params => {
            let taskId = params['taskId'];

            if(taskId!=null){
                this.initializeEditForm(taskId);
            }
            else{
                this.router.navigate(['/userHome']);
            }
        });
    }

    onSubmit() {
        this.fail=false;
        let task=this.convertFormGroupToTask();
        console.log('EDYTOWANIE ZADANIA');
        console.log(JSON.stringify(task));
        this.taskService.editTask(task).subscribe(response => {
            if(response==true){
                this.router.navigate(['/userHome']);
            }
            else{
                this.fail=true;
            }
        });
    }

    convertFormGroupToTask(){
        console.log('CONVERTING TASK');

        console.log("IF this.task.controls.eMailNotificationDate.value is null");
        if(this.task.controls.eMailNotificationDate.value){
            console.log('NO');
        }
        else{
            console.log('YES');
        }

        // return new Task(
        //     this.taskObject.id,
        //     this.task.controls.categoryId.value,
        //     this.authService.currentlyLoggedInUser.username,
        //     this.task.controls.name.value,
        //     this.task.controls.startDate.value==null?null:new Date(this.task.controls.startDate.value),
        //     this.task.controls.duration.value,
        //     TaskStatus.fromStringToEnum(this.task.controls.status.value),
        //     this.task.controls.description.value,
        //     this.task.controls.webNotificationDate.value==null?null:new Date(this.task.controls.webNotificationDate.value),
        //     this.task.controls.eMailNotificationDate.value==null?null:new Date(this.task.controls.eMailNotificationDate.value)
        // )

        return new Task(
            this.taskObject.id,
            this.task.controls.categoryId.value,
            this.authService.currentlyLoggedInUser.username,
            this.task.controls.name.value,
            new Date(this.task.controls.startDate.value),
            this.task.controls.duration.value,
            TaskStatus.fromStringToEnum(this.task.controls.status.value),
            this.task.controls.description.value,
            new Date(this.task.controls.webNotificationDate.value),
            new Date(this.task.controls.eMailNotificationDate.value)
        );
    }

    initializeEditForm(taskId:number){
        console.log('TASK ID PARAMETER');
        console.log(taskId);

        this.taskService.getTaskById(taskId)
            .subscribe(task => {
                console.log(JSON.stringify(task));
                this.taskObject=task;
                if(task!=null){
                    console.log(JSON.stringify(task));
                    this.task.patchValue({
                        name: task.name,
                        categoryId: task.categoryId,
                        startDate: convertDateToDateTimeLocalControlValue(task.startDate),
                        duration: task.duration,
                        status: TaskStatus.fromEnumToString(task.status),
                        description: task.description,
                        webNotificationDate: convertDateToDateTimeLocalControlValue(task.webNotificationDate),
                        eMailNotificationDate: convertDateToDateTimeLocalControlValue(task.eMailNotificationDate)
                    })
                }
                else{
                    this.router.navigate(['/userHome']);
                }
            });
    }
}