import {AbstractControl, ValidatorFn} from "@angular/forms";

export function maxValue(max: Number): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
        const input = control.value,
            isInvalid = input > max;
        if(isInvalid)
            return { 'maxValue': {max} }
        else
            return null;
    };
}

export function minValue(min: Number): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
        const input = control.value,
            isInvalid = input < min;
        if(isInvalid)
            return { 'minValue': {min} }
        else
            return null;
    };
}