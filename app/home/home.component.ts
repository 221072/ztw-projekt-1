import {Component, OnInit} from '@angular/core';

import {UserService} from '../_services/index';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FormNature} from "../_helpers/formNature";
import {AuthenticationService} from "../_services/authentication.service";
import {TaskStatus} from "../_models/taskStatus";
import {TaskService} from "../_services/task.service";
import {Task} from "../_models/task";
import {convertDateToDateTimeLocalControlValue} from "../_helpers/dateTimeConverter";
import {maxValue, minValue} from "../_customValidators/minValue";
import {TaskBoardDate} from "../_models/taskBoardDate";
import {Category} from "../_models/category";
import {CategoryService} from "../_services/category.service";

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {


    constructor(
    ) {
    }

    ngOnInit() {

    }


}