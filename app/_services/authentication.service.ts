﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import {UserRepository} from "../_repositories/userRepository";
import {User} from "../_models/user";
import {UserService} from "./user.service";

@Injectable()
export class AuthenticationService {
    public token: string;
    public currentlyLoggedInUser: User;
    public routeToRedirect: string;

    constructor(private http: Http, private userRepository: UserRepository) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    isAuthenticated(){
        return this.currentlyLoggedInUser!=null;
    }

    login(username: string, password: string): Observable<boolean> {
        return this.http.post('/api/authenticate', JSON.stringify({ username: username, password: password }))
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));

                    // this.userService.getUser(username)
                    //     .subscribe(response => {
                    //         this.currentlyLoggedInUser=JSON.parse(response));
                    //     });

                    this.currentlyLoggedInUser=this.userRepository.getUser(username, password);

                    // this.userRepository.getUser(username)
                    //     .subscribe(response => {
                    //         console.log(response);
                    //         this.currentlyLoggedInUser=response;
                    //
                    //         return true;
                    //     });

                    //this.currentlyLoggedInUser=this.userRepository.findUser(username);

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}