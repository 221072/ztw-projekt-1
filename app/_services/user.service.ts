﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './index';
import {User} from "../_models/user";


@Injectable()
export class UserService {
    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {
    }

    getUsers(): Observable<User[]> {
        // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        // get users from api
        return this.http.get('/api/users', options)
            .map((response: Response) => response.json());
    }

    getUser(username:string): Observable<User>{
        // add authorization header with jwt token
        console.log('USER SERVICE ');
        console.log(username);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.get('/api/users/'+username, options)
            .map((response: Response) => response.json());
    }

    deleteUser(username:string): Observable<User>{
        // add authorization header with jwt token
        console.log('DELETE USER SERVICE ');
        console.log(username);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.delete('/api/users/'+username, options)
            .map((response: Response) => response.json());
    }

    addUser(user:User): Observable<Boolean>{
        console.log('ADD USER SERVICE ');
        console.log(user);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.post('/api/users', JSON.stringify(user), options)
            .map((response: Response) => {
                return response.status == 200;
            });
    }

    editUser(user:User): Observable<Boolean>{
        console.log('EDIT USER SERVICE ');
        console.log(user);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.put('/api/users', JSON.stringify(user), options)
            .map((response: Response) => {
                return response.status == 200;
            });
    }
}