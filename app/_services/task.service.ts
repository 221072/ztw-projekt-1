import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './index';
import {Task} from "../_models/task";
import {User} from "../_models/user";
@Injectable()
export class TaskService{
    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {
    }

    // getTasks(date:Date): Observable<Task[]>{
    //     console.log('TASK SERVICE ');
    //     console.log(date);
    //
    //     let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //     let options = new RequestOptions({ headers: headers });
    //
    //     return this.http.get('/api/tasks/'+date.getUTCDate(), options)
    //         .map((response: Response) => response.json());
    // }

    addTask(task:Task):Observable<Boolean>{
        let taskString=JSON.stringify(task);

        console.log('ADD TASK SERVICE');
        console.log(taskString);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.post('/api/tasks', taskString, options)
            .map((response: Response) => {
                return response.status == 200;
            });
    }

    editTask(task:Task):Observable<Boolean>{
        let taskString=JSON.stringify(task);

        console.log('EDIT TASK SERVICE');
        console.log(taskString);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.put('/api/tasks', taskString, options)
            .map((response: Response) => {
                return response.status == 200;
            });
    }

    deleteTaskById(taskId:number):Observable<Boolean>{
        console.log('DELETE TASK SERVICE');
        console.log(taskId);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.delete('/api/tasks/id/'+taskId, options)
            .map((response: Response) => {
                return response.status == 200;
            });
    }

    getTaskById(taskId:number):Observable<Task>{
        console.log('API: '+'/api/tasks/id/'+taskId);

        console.log('GET TASK BY ID SERVICE ');
        console.log(taskId);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.get('/api/tasks/id/'+taskId, options)
            .map((response: Response) => response.json());
    }

    getTasksByUsername(username:string): Observable<Task[]>{
        console.log('GET TASKS BY USERNAME SERVICE ');
        console.log(username);

        console.log('URL');
        console.log('/api/tasks/username/'+username);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.get('/api/tasks/username/'+username, options)
            .map((response: Response) => response.json());
    }

    getTasksByWeek(date:Date): Observable<Task[]>{
        let dateString=JSON.stringify(date);

        console.log('GET TASK BY WEEK SERVICE ');
        console.log(dateString);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.get('/api/tasks/week/'+dateString, options)
            .map((response: Response) => response.json());
    }

    getTasksWebNotifications(username:String): Observable<Task[]>{
        ///\/api\/tasksWebNotification\/username\/\w+$/
        console.log('GET TASKS WEB NOTIFICATION BY USERNAME SERVICE ');
        console.log(username);

        console.log('URL');
        console.log('/api/tasksWebNotification/username/'+username);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.get('/api/tasksWebNotification/username/'+username, options)
            .map((response: Response) => response.json());
    }
}