import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './index';
import {Category} from "../_models/category";


@Injectable()
export class CategoryService {
    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {
    }

    getCategories(): Observable<Category[]> {
        // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        console.log('ALL CATEGORIES SERVICE');

        // get users from api
        return this.http.get('/api/categories', options)
            .map((response: Response) => response.json());
    }

    getCategoryById(categoryId:number): Observable<Category>{
        // add authorization header with jwt token
        console.log('CATEGORY SERVICE ');
        console.log(categoryId);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.get('/api/categories/id/'+categoryId, options)
            .map((response: Response) => response.json());
    }

    // deleteCategory(categoryId:string): Observable<Category>{
    //     // add authorization header with jwt token
    //     console.log('DELETE CATEGORY SERVICE ');
    //     console.log(categoryId);
    //
    //     let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    //     let options = new RequestOptions({ headers: headers });
    //
    //     return this.http.delete('/api/categories/'+categoryId, options)
    //         .map((response: Response) => response.json());
    // }

    addCategory(category:Category): Observable<Boolean>{
        console.log('ADD CATEGORY SERVICE ');
        console.log(category);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.post('/api/categories', JSON.stringify(category), options)
            .map((response: Response) => {
                return response.status == 200;
            });
    }

    editCategory(category:Category): Observable<Boolean>{
        console.log('EDIT CATEGORY SERVICE ');
        console.log(category);

        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        return this.http.put('/api/categories', JSON.stringify(category), options)
            .map((response: Response) => {
                console.log(response.status);
                return response.status == 200;
            });
    }
}