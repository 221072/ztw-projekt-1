import {Component, OnInit} from '@angular/core';

import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../_services/authentication.service";
import {convertDateToDateTimeLocalControlValue} from "../_helpers/dateTimeConverter";
import {maxValue, minValue} from "../_customValidators/minValue";
import {Category} from "../_models/category";
import {CategoryService} from "../_services/category.service";
import {Color} from "../_models/color";

@Component({
    moduleId: module.id,
    templateUrl: 'editCategory.component.html'
})

export class EditCategoryComponent implements OnInit {
    category: FormGroup;
    categoryObject: Category;

    fail:boolean;

    colors:string[];

    constructor(
        private router:Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private categoryService: CategoryService
    ) {
    }

    ngOnInit() {
        this.colors=Color.toStringArray();

        this.category=this.formBuilder.group({
            name: ['',[Validators.required, Validators.maxLength(30)]],
            description: ['',[]],
            color: [Color.fromEnumToString(Color.White),[Validators.required]]
        });

        this.route.params.subscribe(params => {
            let categoryId = params['categoryId'];

            if(categoryId!=null){
                this.initializeEditForm(categoryId);
            }
            else{
                this.router.navigate(['/categoryManagement']);
            }
        });
    }

    onSubmit() {
        this.fail=false;
        let category=this.convertFormGroupToCategory();
        this.categoryService.editCategory(category).subscribe(response => {
            if(response==true){
                this.router.navigate(['/categoryManagement']);
            }
            else{
                this.fail=true;
            }
        });
    }

    convertFormGroupToCategory(){
        console.log('CONVERTING TASK');

        return new Category(
            this.categoryObject.categoryId,
            this.category.controls.name.value,
            this.category.controls.description.value,
            Color.fromStringToEnum(this.category.controls.color.value)
        )
    }

    initializeEditForm(categoryId:number){
        console.log('CATEGORY ID PARAMETER');
        console.log(categoryId);

        this.categoryService.getCategoryById(categoryId)
            .subscribe(category => {
                console.log(JSON.stringify(category));
                this.categoryObject=category;
                if(category!=null){
                    console.log(JSON.stringify(category));
                    this.category.patchValue({
                        name: category.name,
                        description: category.description,
                        color: Color.fromEnumToString(category.color)
                    })
                }
                else{
                    this.router.navigate(['/categoryManagement']);
                }
            });
    }
}