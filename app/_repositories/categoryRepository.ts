import {Injectable} from "@angular/core";
import {Category} from "../_models/category";
import {Color} from "../_models/color";

@Injectable()
export class CategoryRepository{
    constructor() {
        if(JSON.parse(localStorage.getItem('insertedCategories'))!=true){
            console.log("ADDING EXAMPLE CATEGORIES TO REPOSITORY");

            let categories=[
                new Category(1,'dokumentacja','dokumentacja dla inżynierów',Color.White),
                new Category(2,'testy','testowanie oprogramowania', Color.Silver),
                new Category(3,'programowanie','wytwarzanie oprogramowania',Color.Grey)
            ];

            this.writeCategories(categories);
            this.writeCategoryId(3);

            console.log("ADDING EXAMPLE CATEGORIES TO REPOSITORY FINISHED");
        }
        else{
            console.log('NOT EMPTY CAT');
        }
    }

    public getCategoryById(categoryId:number){
        let categories=this.readCategories();

        for(let category of categories){
            if(category.categoryId==categoryId){
                return category;
            }
        }

        return null;
    }

    // public deleteCategory(categoryId:number){
    //     let categories=this.readCategories();
    //     let categoriesAfterDelete=[];
    //
    //     for(let category of categories){
    //         if(category.categoryId!=categoryId){
    //             categoriesAfterDelete.push(category);
    //         }
    //     }
    //
    //     this.writeCategories(categoriesAfterDelete);
    // }

    public editCategory(categoryToEdit:Category){
        let categories=this.readCategories();
        let categoriesAfterEdit=[];

        for(let category of categories){
            if(categoryToEdit.categoryId!=category.categoryId){
                categoriesAfterEdit.push(category);
            }
            else {
                categoriesAfterEdit.push(categoryToEdit);
            }
        }

        this.writeCategories(categoriesAfterEdit);
    }

    public addCategory(categoryToAdd:Category){
        for(let category of this.readCategories()){
            if(categoryToAdd.name==category.name) {
                return false;
            }
        }

        let lastCategoryId=this.readLastCategoryId();
        let categoryId=lastCategoryId+1;
        this.writeCategoryId(categoryId);

        categoryToAdd.categoryId=categoryId;

        let categories=this.readCategories();
        categories.push(categoryToAdd);
        this.writeCategories(categories);

        return true;
    }

    private readLastCategoryId(){
        return JSON.parse(localStorage.getItem('categoryId'));
    }

    private writeCategoryId(categoryId:number){
        localStorage.setItem('categoryId',JSON.stringify(categoryId));
    }

    public readCategories():Category[]{
        return JSON.parse(localStorage.getItem('categories'));
    }

    private writeCategories(categories:Category[]){
        localStorage.setItem('insertedCategories',JSON.stringify(true));
        localStorage.setItem('categories',JSON.stringify(categories));
    }
}