import {Injectable} from "@angular/core";
import {Task} from "../_models/task";
import {UserRepository} from "./userRepository";
import {TaskStatus} from "../_models/taskStatus";
@Injectable()
export class TaskRepository{
    constructor() {
        if(JSON.parse(localStorage.getItem('insertedTasks'))!=true){
            console.log('TASK REPOSITORY CONSTRUCTOR START');

            let tasks:Task[]=[
                new Task(1,1,'user','zadanie 1',new Date('2017-04-24T12:00:00'),2,TaskStatus.Complete,'opis zadania 1',new Date('2017-04-24T12:00:00'),new Date('2017-04-24T12:00:00')),
                new Task(2,2,'user','zadanie 2',new Date('2017-04-25T16:00:00'),6,TaskStatus.InProgress,'opis zadania 2',new Date('2017-04-24T12:00:00'),new Date('2017-04-24T12:00:00')),
                new Task(3,3,'user','zadanie 3',new Date('2017-04-26T10:00:00'),4,TaskStatus.New,'opis zadania 3',new Date('2017-04-24T12:00:00'),new Date('2017-04-24T12:00:00'))
            ];

            console.log('TASK REPOSITORY CONSTRUCTOR SAVE START');
            localStorage.setItem('tasks', JSON.stringify(tasks));
            console.log('TASK REPOSITORY CONSTRUCTOR SAVE END');
        }
        else{
            console.log('NOT EMPTY TASKS');
        }
    }

    public addTask(newTask:Task){
        let lastId=this.readId();
        let newId=lastId+1;
        this.updateId(newId);

        newTask.id=newId;

        let tasks=this.readTasks();
        tasks.push(newTask);
        this.updateTasks(tasks);
    }

    public editTask(taskToEdit:Task){
        let tasks=[];
        for(let task of this.readTasks()){
            if(task.id!=taskToEdit.id){
                tasks.push(task);
            }
            else{
                tasks.push(taskToEdit);
            }
        }
        this.updateTasks(tasks);
    }

    public deleteTaskById(id:number){
        let tasks=[];
        for(let task of this.readTasks()){
            if(task.id!=id){
                tasks.push(task);
            }
        }
        this.updateTasks(tasks);
    }

    public getTaskById(id:number){
        console.log('GET TASK BY ID START');
        let tasks=this.readTasks();
        console.log('GET TASK BY ID');
        for(let task of tasks){
            console.log(JSON.stringify(task));
            if(task.id==id){
                return task;
            }
        }
        return null;
    }

    public getTasksByUsername(username: string){
        console.log('GET TASK BY USERNAME START');
        let tasks=this.readTasks();
        let foundTasks=[];
        console.log('GET TASK BY USERNAME');
        for(let task of tasks){
            console.log(JSON.stringify(task));
            if(task.username==username){
                foundTasks.push(task);
            }
        }
        return foundTasks;
    }

    public getTasksWebNotificationByUsername(username:string){
        console.log('GET TASK WEB NOTIFICATION START');
        let tasks=this.readTasks();
        let foundTasks=[];
        console.log('GET TASK WEB NOTIFICATION');
        for(let task of tasks){
            //console.log(JSON.stringify(tasks));

            let currentDate=new Date();
            let currentDay=currentDate.getDate();
            let currentMonth=currentDate.getMonth();
            let currentYear=currentDate.getFullYear();
            //let currentHour=currentDate.getHours();
            //let currentMinute=currentDate.getMinutes();

            console.log('current date: '+currentDate.toString());
            console.log('web notification date: '+task.webNotificationDate.toString());

            let webNotDate=new Date(task.webNotificationDate);

            console.log('webnot_day '+webNotDate.getDate()+'  '+currentDay);
            console.log('webnot_month '+webNotDate.getMonth()+'  '+currentMonth);
            console.log('webnot_year '+webNotDate.getFullYear()+'  '+currentYear);

            if(
                task.username==username &&
                webNotDate.getDate()==currentDay &&
                webNotDate.getMonth()==currentMonth &&
                webNotDate.getFullYear()==currentYear
                //task.webNotificationDate.getHours()==currentHour &&
                //task.webNotificationDate.getMinutes()==currentMinute
            ){
                console.log('ZNALEZIONY!');
                foundTasks.push(task);
            }
        }
        console.log('web notification tasks: ');
        console.log(JSON.stringify(foundTasks));
        return foundTasks;
    }

    public getTasksByWeek(date: Date){
        let dateRange=this.getDateRange(date);
        let sinceDate=dateRange[0];
        let toDate=dateRange[1];

        let tasks=this.readTasks();
        let foundTasks=[];

        for(let task of tasks){
            if(task.startDate>=sinceDate && task.startDate<=toDate){
                foundTasks.push(task);
            }
        }

        return foundTasks;
    }

    public readTasks(): Task[] {
        return JSON.parse(localStorage.getItem('tasks'));
    }

    private updateTasks(tasks:Task[]){
        localStorage.setItem('insertedTasks',JSON.stringify(true));
        localStorage.setItem('tasks', JSON.stringify(tasks));
    }

    public readId():number{
        return JSON.parse(localStorage.getItem('taskId'));
    }

    private updateId(id:number){
        localStorage.setItem('taskId', JSON.stringify(id));
    }

    public getDateRange(date:Date){
        let dayNumber=date.getDay();
        let toNumber=6-dayNumber;

        let sinceDate=new Date();
        sinceDate.setHours(0,0,0);
        sinceDate.setDate(date.getDate() - dayNumber);

        let toDate=new Date();
        toDate.setHours(0,0,0);
        toDate.setDate(date.getDate() + toNumber);

        return [sinceDate,toDate];
    }
}