// import {User} from "../_models/user";
// import {Role} from "../_models/role";
// var users:User[]=[
//     new User('admin','admin','admin@wp.pl',Role.Admin),
//     new User('user','user','user@wp.pl',Role.User)
// ];

import {Injectable} from "@angular/core";
import {User} from "../_models/user";
import {Role} from "../_models/role";
@Injectable()
export class UserRepository {
    constructor() {
        if (JSON.parse(localStorage.getItem('insertedUsers')) != true) {
            console.log('USER REPOSITORY CONSTRUCTOR START');

            let users: User[] = [
                new User('admin', 'admin', 'admin@wp.pl', Role.Admin),
                new User('user', 'user', 'user@wp.pl', Role.User)
            ];

            console.log('USER REPOSITORY CONSTRUCTOR SAVE START');
            localStorage.setItem('users', JSON.stringify(users));
            console.log('USER REPOSITORY CONSTRUCTOR SAVE END');
        }
        else {
            console.log('NOT EMPTY USERS');
        }
    }

    public getUser(username: string, password: string) {
        let users=this.readUsers();

        console.log('GET USER: '+username+' haslo: '+password);
        console.log(JSON.stringify(users));

        for (let user of users) {
            console.log('USER GETUSER:');
            console.log(JSON.stringify(user));

            if(user.username==username && user.password==password){
                return user;
            }

            // if (user.isValidUser(username, password)) {
            //     return user;
            // }
        }
        return null;
    }

    public findUser(username: string) {
        let users = this.readUsers();

        console.log('FIND USER');
        console.log(JSON.stringify(users));

        for (let user of users) {
            if (user.username === username) {
                return user;
            }
        }
        return null;
    }

    public deleteUser(username: string){
        let users=[];
        for(let user of this.readUsers()){
            if(user.username!=username){
                users.push(user);
            }
        }
        this.updateUsers(users);
    }

    public addUser(user: User) {
        if (this.findUser(user.username)) {
            return false;
        }
        else {
            let users=this.readUsers();
            users.push(user);
            this.updateUsers(users);

            return true;
        }
    }

    public editUser(newUser: User) {
        var oldUser = this.findUser(newUser.username);
        if (oldUser != null) {
            oldUser.eMail = newUser.eMail;
            oldUser.password = newUser.password;
            oldUser.role = newUser.role;
            this.deleteUser(oldUser.username);
            this.addUser(oldUser);
            return true;
        }
        else {
            return false;
        }
    }

    public readUsers(): User[] {
        return JSON.parse(localStorage.getItem('users'));
    }

    private updateUsers(users:User[]){
        localStorage.setItem('insertedUsers',JSON.stringify(true));
        localStorage.setItem('users', JSON.stringify(users));
    }

    public readId():number{
        return JSON.parse(localStorage.getItem('userId'));
    }

    private updateId(id:number){
        localStorage.setItem('userId', JSON.stringify(id));
    }
}