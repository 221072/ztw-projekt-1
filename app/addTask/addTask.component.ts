import {Component, OnInit} from '@angular/core';

import {UserService} from '../_services/index';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FormNature} from "../_helpers/formNature";
import {AuthenticationService} from "../_services/authentication.service";
import {TaskStatus} from "../_models/taskStatus";
import {TaskService} from "../_services/task.service";
import {Task} from "../_models/task";
import {convertDateToDateTimeLocalControlValue} from "../_helpers/dateTimeConverter";
import {maxValue, minValue} from "../_customValidators/minValue";
import {TaskBoardDate} from "../_models/taskBoardDate";
import {Category} from "../_models/category";
import {CategoryService} from "../_services/category.service";
import {Observable} from "rxjs/Observable";

@Component({
    moduleId: module.id,
    templateUrl: 'addTask.component.html'
})

export class AddTaskComponent implements OnInit {
    task: FormGroup;

    fail:boolean;

    categories:Category[];

    statusEnum=TaskStatus;
    statuses:string[];

    constructor(
        private router:Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private taskService: TaskService,
        private authService: AuthenticationService,
        private categoryService: CategoryService
    ) {
    }

    ngOnInit() {
        this.statuses=TaskStatus.toStringArray();

        //let categoriesPromise = this.categoryService.getCategories();

        this.task=this.formBuilder.group({
            name: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
            categoryId: ['',[Validators.required]],
            startDate: ['', []],
            duration: ['', [minValue(0),maxValue(20)]],
            status: [TaskStatus.fromEnumToString(TaskStatus.New),[Validators.required]],
            description: ['',[]],
            webNotificationDate: ['', []],
            eMailNotificationDate: ['', []]
        });


        this.categoryService.getCategories()
            .subscribe(response => {
                console.log('ALL CATEGORIES');
                this.categories=response;
                console.log(JSON.stringify(response));

                this.route.params.subscribe(params => {
                    let date = params['date'];

                    console.log('DATE PARAMETER');
                    console.log(date);

                    if(date!=null){
                        this.initializeAddForm(date);
                    }
                    else{
                        this.router.navigate(['/userHome']);
                    }
                });
            });
    }

    onSubmit() {
        this.fail=false;
        let task=this.convertFormGroupToTask();
        console.log('DODAWANIE TASKU: ');
        console.log(task);
        this.taskService.addTask(task).subscribe(response => {
            if(response==true){
                this.router.navigate(['/userHome']);
            }
            else{
                this.fail=true;
            }
        });
    }

    convertFormGroupToTask(){
        console.log('CONVERTING TASK');

        return new Task(
            null,
            this.task.controls.categoryId.value,
            this.authService.currentlyLoggedInUser.username,
            this.task.controls.name.value,
            new Date(this.task.controls.startDate.value),
            this.task.controls.duration.value,
            TaskStatus.fromStringToEnum(this.task.controls.status.value),
            this.task.controls.description.value,
            new Date(this.task.controls.webNotificationDate.value),
            new Date(this.task.controls.eMailNotificationDate.value)
        )
    }

    private initializeAddForm(date: any) {
        console.log('date PARAMETER');
        console.log(date);

        console.log('CATEGORIES ARRAY');
        console.log(this.categories);

        this.task.patchValue({
            categoryId: this.categories[0].categoryId,
            startDate: convertDateToDateTimeLocalControlValue(date),
        })
    }
}