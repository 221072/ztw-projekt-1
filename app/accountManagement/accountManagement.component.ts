import { Component, OnInit } from '@angular/core';

import { UserService } from '../_services/index';
import { User } from "../_models/user";
import {Router} from "@angular/router";
import {Role} from "../_models/role";

@Component({
    moduleId: module.id,
    templateUrl: 'accountManagement.component.html'
})

export class AccountManagementComponent implements OnInit {
    users: User[] = [];
    deleted:boolean=false;
    roleEnum=Role;

    constructor(
        private router:Router,
        private userService: UserService) { }

    ngOnInit() {
        this.userService
            .getUsers()
            .subscribe(users => {
                this.users = users;
            });
    }

    deleteUser(username:string, idx:number){
        this.userService
            .deleteUser(username)
            .subscribe(users => {
                this.deleted=true;
            });

        this.userService
            .getUsers()
            .subscribe(users => {
                this.users = users;
            });
    }

    editUser(username:string){
        this.router.navigate(['/accountForm/'+username]);
    }
}