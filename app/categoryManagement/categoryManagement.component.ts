import {Component, OnInit} from "@angular/core";
import {Category} from "../_models/category";
import {CategoryService} from "../_services/category.service";
import {Router} from "@angular/router";
import {Color} from "../_models/color";
/**
 * Created by bartek on 4/28/17.
 */
@Component({
    moduleId: module.id,
    templateUrl: 'categoryManagement.component.html'
})
export class CategoryManagementComponent implements OnInit {
    categories: Category[] = [];
    getCategoryByIdApiTest: Category;
    colors:string[];

    constructor(
        private router:Router,
        private categoryService:CategoryService) { }

    ngOnInit() {
        this.colors=Color.toStringArray();

        this.categoryService.getCategories()
            .subscribe(categories => {
                console.log('CATEGORIES COMPONENT: ALL');
                console.log(JSON.stringify(categories));
                this.categories = categories;
            });
    }

    editCategory(categoryId: number){
        this.router.navigate(['/editCategory/'+categoryId]);
    }
}