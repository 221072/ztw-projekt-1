﻿import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

import { AuthenticationService } from '../_services/index';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Role} from "../_models/role";
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {
    user: FormGroup;
    loading = false;
    incorrectUser = false;

    ticks = 0;
    private timer:any;
    // Subscription object
    private sub: Subscription;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private formBuilder:FormBuilder
    ){
    }

    ngOnInit() {
        this.user=this.formBuilder.group({
            username: ['',[Validators.required]],
            password: ['',[Validators.required]]
        });
        //this.authenticationService.routeToRedirect = this.router.url;
        // reset login status
        this.authenticationService.logout();

        this.timer = Observable.timer(2000,5000);
        // subscribing to a observable returns a subscription object
        this.sub = this.timer.subscribe(t => this.tickerFunc(t));
    }

    tickerFunc(tick:any){
        console.log(this);
        this.ticks = tick
    }

    ngOnDestroy(){
        console.log("Destroy timer");
        // unsubscribe here
        this.sub.unsubscribe();

    }


// onSubmit(){
    //     console.log(this.user.value.username);
    // }

    onSubmit() {
        console.log(this.user.value.username);

        //this.loading = true;
        this.authenticationService.login(this.user.value.username, this.user.value.password)
            .subscribe(result => {
                if (result === true) {
                    console.log('success');

                    switch (this.authenticationService.currentlyLoggedInUser.role){
                        case Role.Admin:
                            console.log(this.router.url);
                            this.router.navigate(['/adminHome']);
                            break;
                        case Role.User:


                            this.router.navigate(['/userHome']);
                            // this.router.events
                            //     .filter(event => event instanceof NavigationEnd)
                            //     .subscribe(e => {
                            //         console.log('prev:', this.previousUrl);
                            //         this.previousUrl = e.url;
                            //     });
                            break;
                        default:
                            this.incorrectUser=true;
                            break;
                    }
                } else {
                    this.incorrectUser = true;
                    this.loading = false;
                }
            });
    }
}
