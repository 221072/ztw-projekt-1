import {Component, OnInit} from '@angular/core';

import {UserService} from '../_services/index';
import {User} from "../_models/user";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {passwordMatcher} from "../_customValidators/password-matcher";
import {FormNature} from "../_helpers/formNature";
import {Role} from "../_models/role";
import {AuthenticationService} from "../_services/authentication.service";

@Component({
    moduleId: module.id,
    templateUrl: 'accountForm.component.html'
})

export class AccountFormComponent implements OnInit {
    user: FormGroup;
    formNature:FormNature;
    success:boolean;
    fail:boolean;
    roleEnum=Role;
    formNatureEnum=FormNature;
    roles:string[];
    currentlyLoggedInUser:User;

    constructor(
        private router:Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private userService: UserService,
        private authService: AuthenticationService
    ) {
    }

    ngOnInit() {
        this.roles=Role.toStringArray();
        this.currentlyLoggedInUser=this.authService.currentlyLoggedInUser;
        this.formNature=FormNature.Add;
        this.user=this.formBuilder.group({
            username: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
            eMail: ['',[Validators.required,Validators.pattern( /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
            role:[Role.fromEnumToString(Role.User),[Validators.required]],
            passwords: this.formBuilder.group({
                password: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
                passwordConfirm: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(30)]]
            }, { validator: passwordMatcher })
        });

        this.route.params.subscribe(params => {
            let username = params['username'];

            console.log("USERNAME PARAMETER: ");
            console.log(username);

            if(username!=null){
                this.formNature=FormNature.Edit;
                this.userService.getUser(username)
                    .subscribe(user => {
                        if(user!=null){
                            console.log(JSON.stringify(user));
                            this.user.patchValue({
                                username: user.username,
                                eMail:user.eMail,
                                role:Role.fromEnumToString(user.role),
                                passwords:{
                                    password:user.password,
                                    passwordConfirm:user.password
                                }
                            })
                        }
                    });
            }
        });

        if(this.formNature as FormNature===FormNature.Edit){
            let usernameControl=this.user.get('username');
            usernameControl.disable();
        }
    }

    onSubmit() {
        this.success=null;
        this.fail=null;
        let user=this.convertFormGroupToUser();
        switch (this.formNature){
            case FormNature.Add:
                this.userService.addUser(user).subscribe(response => {
                    if(response==true){
                        this.success=true;
                        this.router.navigate(['/userHome']);
                    }
                    else{
                        this.fail=true;
                    }
                });
                break;
            case FormNature.Edit:
                this.userService.editUser(user).subscribe(response => {
                    if(response==true){
                        this.router.navigate(['/accountManagement']);
                    }
                    else{
                        this.fail=false;
                    }
                });
                break;
        }
    }

    convertFormGroupToUser(){
        let passwords=this.user.controls.passwords;
        return new User(
            this.user.controls.username.value,
            passwords.get('password').value,
            this.user.controls.eMail.value,
            Role.fromStringToEnum(this.user.controls.role.value)
        );
    }



}