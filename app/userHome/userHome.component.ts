import { Component, OnInit } from '@angular/core';

import { UserService } from '../_services/index';
import { User } from "../_models/user";
import {PolishDay, PolishMonth, TaskBoardDate} from "../_models/taskBoardDate";
import {TaskBoard, TaskBoardDay} from "../_models/taskBoard";
import {TaskService} from "../_services/task.service";
import {AuthenticationService} from "../_services/authentication.service";
import {NavigationExtras, Router} from "@angular/router";
import {TaskStatus} from "../_models/taskStatus";
import {Color} from "../_models/color";
import {Observable} from "rxjs/Rx";
import {Task} from "../_models/task";

@Component({
    moduleId: module.id,
    templateUrl: 'userHome.component.html'
})



export class UserHomeComponent implements OnInit {
    taskBoard:TaskBoard;
    date:Date;

    deleteAlert:boolean;
    deleted:boolean;

    taskIdToDelete:number;

    taskStatus=TaskStatus;

    color: Color;

    timer:any;

    webNotificationTasks:Task[];

    constructor(
        private authService:AuthenticationService,
        private taskService: TaskService,
        private router:Router,
    ) {
        this.timer = Observable.timer(5000,5000);
        this.timer.subscribe(t => this.getTasksToWebNotificate());
    }

    getTasksToWebNotificate(){
        console.log('tasks web notification example');
        let currentlyLoggedIn=this.authService.currentlyLoggedInUser;
        this.taskService.getTasksWebNotifications(currentlyLoggedIn.username).subscribe(
            response=>{
                console.log("NOTIFICATION RESULTS: ");
                this.webNotificationTasks=response;
            }
        );
    }

    ngOnInit() {
        this.deleteAlert=false;
        this.deleted=false;

        this.date=new Date();
        this.taskBoard=new TaskBoard();
        this.updateWeek();

        setTimeout(this.getTasksToWebNotificate(),60*1000);
    }

    updateWeek(){
        // let currentlyLoggedIn=this.authService.currentlyLoggedInUser;
        // this.taskService.getTasksByUsername(currentlyLoggedIn.username)
        //     .subscribe(response => {
        //         console.log(response);
        //     });

        let first = this.date.getDate() - this.date.getDay() +1; // First date is the date of the month - the date of the week
        let last = first + 6; // last date is the first date + 6

        console.log("BEGGINING OF THE WEEK");
        console.log(first.toString());

        let firstday = new Date(this.date.setDate(first)).toUTCString();
        //var lastday = new Date(curr.setDate(last)).toUTCString();

        console.log("BEGGINING OF THE WEEK");
        console.log(firstday.toString());

        for(var i=0;i<7;i++){
            let dateString=first+i;
            let date=new Date(this.date.setDate(dateString));

            let monthIdx=date.getMonth();
            let monthName=PolishMonth[monthIdx];

            let dayIdx=date.getDay();
            let dayName=PolishDay[dayIdx];

            let taskBoardDate = new TaskBoardDate(dayName,date.getDate(), monthName,date.getFullYear(),date);
            let taskBoardDay = new TaskBoardDay();
            taskBoardDay.date=taskBoardDate;

            this.taskBoard.addDay(taskBoardDay);
        }
        this.setTasks();
    }

    setTasks(){
        let currentlyLoggedIn=this.authService.currentlyLoggedInUser;
        this.taskService.getTasksByUsername(currentlyLoggedIn.username)
            .subscribe(response => {
                console.log(response);
                this.taskBoard.addTasks(response);
            });
    }
    //
    // goToLastWeek(){
    //     this.moveDaysInDate(-7);
    //     this.updateWeek();
    //
    //     // this.date = new Date(
    //     //     this.date.getFullYear(),
    //     //     this.date.getMonth(),
    //     //     this.date.getDate() - 7
    //     // );
    //     // this.updateWeek();
    //     // this.setTasks();
    // }
    //
    // goToNextWeek(){
    //     this.moveDaysInDate(7);
    //     this.updateWeek();
    //
    //     // this.date = new Date(
    //     //     this.date.getFullYear(),
    //     //     this.date.getMonth(),
    //     //     this.date.getDate() + 7
    //     // );
    //     // this.updateWeek();
    //     // this.setTasks();
    // }

    moveDaysInDate(numberOfDays:any){
        console.log('PREVIOUS DATE');
        //let previousDate=this.date;
        //console.log(JSON.stringify(previousDate));

        //let newDate=new Date(previousDate.getDate()+numberOfDays);
        //this.date.setDate(this.date.getDate()+parseInt(numberOfDays));

        //getTime()

        this.date=new Date(this.date.getTime()+7 * 24 * 60 * 60 * 1000);
        //this.date=newDate;

        //this.date.setDate(this.date.getTime() + 1000 * 60 * 60 * 24* numberOfDays);

        //let timeFromBeggining=this.date.getTime();

        console.log('NEXT DATE');
        console.log(JSON.stringify(this.date));
    }

    addTask(boardDate:TaskBoardDate){
        // let navigationExtras: NavigationExtras = {
        //     queryParams: {
        //         'date':boardDate.date
        //     }
        // };
        this.router.navigate(['/addTask/'+boardDate.date]);
    }

    editTask(taskId:number){
        this.router.navigate(['/editTask/'+taskId]);
    }

    deleteTask(){
        this.taskService.deleteTaskById(this.taskIdToDelete)
            .subscribe(response => {
                this.deleteAlert=false;
                this.ngOnInit();
            });
    }

    openDeleteTaskAlert(taskId:number){
        this.deleteAlert=true;
        this.taskIdToDelete=taskId;
    }
}