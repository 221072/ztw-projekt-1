﻿import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {AuthenticationService} from "../_services/authentication.service";
import {Role} from "../_models/role";

@Injectable()
export class AdminAuthGuard implements CanActivate {

    constructor(
        private authService: AuthenticationService,
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    canActivate() {
        if(this.authService.isAuthenticated()){
            let role=this.authService.currentlyLoggedInUser.role;

            switch (role){
                case Role.User:
                    this.router.navigate(['/login']);
                    return false;
                case Role.Admin:
                    return true;
                default:
                    this.router.navigate(['/login']);
                    return false;
            }
        }
        else{
            this.router.navigate(['/login']);
            return false;
        }
    }
}