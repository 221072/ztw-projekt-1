import {Injectable} from "@angular/core";
import {CanActivate, Router} from "@angular/router";
import {AuthenticationService} from "../_services/authentication.service";
import {Role} from "../_models/role";
@Injectable()
export class UserAuthGuard implements CanActivate {

    constructor(
        private authService:AuthenticationService,
        private router: Router
    ) { }

    canActivate() {
        if(this.authService.isAuthenticated()){
            let role=this.authService.currentlyLoggedInUser.role;

            switch (role){
                case Role.User:
                    return true;
                case Role.Admin:
                    return true;
                default:
                    this.router.navigate(['/login']);
                    return false;
            }
        }
        else{
            this.router.navigate(['/login']);
            return false;
        }
    }
}