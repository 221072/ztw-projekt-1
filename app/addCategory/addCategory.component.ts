import {Component, OnInit} from '@angular/core';

import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../_services/authentication.service";
import {convertDateToDateTimeLocalControlValue} from "../_helpers/dateTimeConverter";
import {maxValue, minValue} from "../_customValidators/minValue";
import {Category} from "../_models/category";
import {CategoryService} from "../_services/category.service";
import {Color} from "../_models/color";

@Component({
    moduleId: module.id,
    templateUrl: 'addCategory.component.html'
})

export class AddCategoryComponent implements OnInit {
    category: FormGroup;

    fail:boolean;

    colors:string[];

    constructor(
        private router:Router,
        private formBuilder: FormBuilder,
        private categoryService: CategoryService
    ) {
    }

    ngOnInit() {
        this.colors=Color.toStringArray();

        this.category=this.formBuilder.group({
            name: ['',[Validators.required, Validators.maxLength(30)]],
            description: ['',[]],
            color: [Color.fromEnumToString(Color.White),[Validators.required]]
        });
    }

    onSubmit() {
        this.fail=false;
        let category=this.convertFormGroupToCategory();
        this.categoryService.addCategory(category).subscribe(response => {
            if(response==true){
                this.router.navigate(['/categoryManagement']);
            }
            else{
                this.fail=true;
            }
        });
    }

    convertFormGroupToCategory(){
        console.log('CONVERTING TASK');

        return new Category(
            null,
            this.category.controls.name.value,
            this.category.controls.description.value,
            Color.fromStringToEnum(this.category.controls.color.value)
        )
    }
}