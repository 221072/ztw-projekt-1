import {TaskBoardDate} from "../_models/taskBoardDate";
export function convertDateToDateTimeLocalControlValue(date:Date){
    //yyyy-MM-ddThh:mm
    if(date){
        date=new Date(date);
        let dateIsoString=date.toISOString();
        let dateArray=dateIsoString.split('T');
        let timeArray=dateArray[1].split(':');
        return dateArray[0]+'T'+timeArray[0]+':'+timeArray[1];
    }
    else{
        return null;
    }

}

export function convertDateTimeLocalControlValueToDate(date:string){


    // let dateParts=date.split('-');
    // let year=dateParts[0];
    //
    // let month=dateParts[1];
    //
    // let day=dateParts[2].substr(0,2);
    //
    // let timeBeginIdx=dateParts[2].indexOf('T');
    // let hour=dateParts[2].substr(timeBeginIdx+1,2);
    // let minute=dateParts[]
}