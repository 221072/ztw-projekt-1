﻿import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import {UserRepository} from "../_repositories/userRepository";
import {Role} from "../_models/role";
import {User} from "../_models/user";
import {Task} from "../_models/task";
import {TaskRepository} from "../_repositories/taskRepository";
import {CategoryRepository} from "../_repositories/categoryRepository";
import {Category} from "../_models/category";

export function fakeBackendFactory(backend: MockBackend, options: BaseRequestOptions, taskRepository: TaskRepository, categoryRepository: CategoryRepository, userRepository:UserRepository) {
    // configure fake backend
    backend.connections.subscribe((connection: MockConnection) => {
        //let testUser = { username: 'test', password: 'test', firstName: 'Test', lastName: 'User' };

        // wrap in timeout to simulate server api call
        setTimeout(() => {

            // fake authenticate api end point
            if (connection.request.url.endsWith('/api/authenticate') && connection.request.method === RequestMethod.Post) {
                // get parameters from post request
                let params = JSON.parse(connection.request.getBody());

                let user = userRepository.getUser(params.username, params.password);

                // check user credentials and return fake jwt token if valid
                if (user!=null) {

                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200, body: { token: 'fake-jwt-token' } })
                    ));
                } else {
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200 })
                    ));
                }
            }

            // fake users api end point
            if (connection.request.url.endsWith('/api/users') && connection.request.method === RequestMethod.Get) {
                // check for fake auth token in header and return test users if valid, this security is implemented server side
                // in a real application
                if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    console.log(JSON.stringify(userRepository.readUsers()));

                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200, body: userRepository.readUsers() })
                    ));
                } else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 401 })
                    ));
                }
            }

            if (connection.request.url.match(/\/api\/users\/\w+$/) && connection.request.method === RequestMethod.Get) {
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                console.log('USERS GET');
                if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find user by id in users array
                    let urlParts = connection.request.url.split('/');
                    let username = urlParts[urlParts.length - 1];

                    console.log('USERS GET');
                    console.log(username);

                    let user=userRepository.findUser(username);

                    // respond 200 OK with user
                    connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: user })));
                } else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
                }
                return;
            }

            if (connection.request.url.match(/\/api\/users\/\w+$/) && connection.request.method === RequestMethod.Delete) {
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find user by id in users array
                    let urlParts = connection.request.url.split('/');
                    let username = urlParts[urlParts.length - 1];

                    userRepository.deleteUser(username);

                    // respond 200 OK with user
                    connection.mockRespond(new Response(new ResponseOptions({ status: 200 })));
                } else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
                }
                return;
            }

            if (connection.request.url.endsWith('/api/users') && connection.request.method === RequestMethod.Post) {
                let params = JSON.parse(connection.request.getBody());
                let user=new User(params.username,params.password,params.eMail,params.role);
                console.log('adding user:');
                console.log(JSON.stringify(user));
                let added = userRepository.addUser(user);
                console.log('users');
                console.log(JSON.stringify(userRepository.readUsers()));

                if(added){
                    connection.mockRespond(new Response(new ResponseOptions({ status: 200 })));
                }
                else{
                    connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
                }
            }

            if (connection.request.url.endsWith('/api/users') && connection.request.method === RequestMethod.Put) {
                let params = JSON.parse(connection.request.getBody());
                let user=new User(params.username,params.password,params.eMail,params.role);
                let edited = userRepository.editUser(user);

                if(edited){
                    connection.mockRespond(new Response(new ResponseOptions({ status: 200 })));
                }
                else{
                    connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
                }
            }

            // if (connection.request.url.match(/\/api\/tasks\/\D+$/) && connection.request.method === RequestMethod.Get) {
            //     // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
            //     if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
            //         // find user by id in users array
            //         let urlParts = connection.request.url.split('/');
            //         let dateString = urlParts[urlParts.length - 1];
            //
            //         let date=new Date(dateString);
            //
            //         let users = TaskRepository.getTasksByWeek(date);
            //
            //         // respond 200 OK with user
            //         connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: users })));
            //     } else {
            //         // return 401 not authorised if token is null or invalid
            //         connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
            //     }
            //     return;
            // }

            if (connection.request.url.match(/\/api\/tasks\/id\/\d+$/) && connection.request.method === RequestMethod.Get) {
                if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    let urlParts = connection.request.url.split('/');
                    let taskId = parseInt(urlParts[urlParts.length - 1]);

                    console.log('API: /api/tasks/id/'+taskId);

                    let task=taskRepository.getTaskById(taskId);

                    console.log('TASK: ');
                    console.log(JSON.stringify(task));

                    if(task==null){
                        connection.mockRespond(new Response(new ResponseOptions({ status: 404 })));
                    }
                    else{
                        connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: task })));
                    }
                }
                else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
                }
                return;
            }

            if (connection.request.url.match(/\/api\/tasks\/week\/\w+$/) && connection.request.method === RequestMethod.Get) {
                if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    let urlParts = connection.request.url.split('/');
                    let dateString = urlParts[urlParts.length - 1];
                    let date = new Date(dateString);
                    let tasks=taskRepository.getTasksByWeek(date);

                    if(tasks==null){
                        connection.mockRespond(new Response(new ResponseOptions({ status: 404 })));
                    }
                    else{
                        connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: tasks })));
                    }
                }
                else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
                }
                return;
            }
            if (connection.request.url.match(/\/api\/tasks\/username\/\w+$/) && connection.request.method === RequestMethod.Get) {
                if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    let urlParts = connection.request.url.split('/');
                    let username = urlParts[urlParts.length - 1];
                    let tasks=taskRepository.getTasksByUsername(username);
                    let tasksString=JSON.stringify(tasks);

                    console.log('GET TASKS BY USERNAME BACKEND');
                    console.log(tasksString);

                    if(tasks==null){
                        connection.mockRespond(new Response(new ResponseOptions({ status: 404 })));
                    }
                    else{
                        connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: tasks })));
                    }
                }
                else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
                }
                return;
            }

            if (connection.request.url.endsWith('/api/tasks') && connection.request.method === RequestMethod.Post) {
                let params = JSON.parse(connection.request.getBody());

                let task = Task.convertFromResponseToTask(params);

                console.log('adding task:');
                console.log(JSON.stringify(task));

                taskRepository.addTask(task);

                console.log('tasks');
                console.log(JSON.stringify(taskRepository.readTasks()));

                connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: task })))
            }

            if (connection.request.url.endsWith('/api/tasks') && connection.request.method === RequestMethod.Put) {
                let params = JSON.parse(connection.request.getBody());

                let task=Task.convertFromResponseToTask(params);

                console.log('editing task:');
                console.log(JSON.stringify(task));

                taskRepository.editTask(task);

                console.log('tasks');
                console.log(JSON.stringify(taskRepository.readTasks()));

                connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: task })))
            }

            if (connection.request.url.match(/\/api\/tasks\/id\/\d+$/) && connection.request.method === RequestMethod.Delete) {
                if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    let urlParts = connection.request.url.split('/');
                    let taskId = parseInt(urlParts[urlParts.length - 1]);

                    console.log('deleting task:');
                    console.log(JSON.stringify(taskId));

                    taskRepository.deleteTaskById(taskId);

                    console.log('tasks');
                    console.log(JSON.stringify(taskRepository.readTasks()));

                    connection.mockRespond(new Response(new ResponseOptions({ status: 200 })));
                }
                else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
                }
                return;
            }

            if (connection.request.url.endsWith('/api/categories') && connection.request.method === RequestMethod.Put) {
                let params = JSON.parse(connection.request.getBody());

                let category=Category.convertFromResponseToCategory(params);

                console.log('editing category:');
                console.log(JSON.stringify(category));

                categoryRepository.editCategory(category);

                console.log('categories');
                console.log(JSON.stringify(categoryRepository.readCategories()));

                connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: category })))
            }

            if (connection.request.url.endsWith('/api/categories') && connection.request.method === RequestMethod.Post) {
                let params = JSON.parse(connection.request.getBody());

                let category=Category.convertFromResponseToCategory(params);

                console.log('adding category:');
                console.log(JSON.stringify(category));

                let result = categoryRepository.addCategory(category);

                console.log('categories');
                console.log(JSON.stringify(categoryRepository.readCategories()));

                if(result){
                    connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: category })))
                }
                else{
                    connection.mockRespond(new Response(new ResponseOptions({ status: 404 })))
                }
            }

            if (connection.request.url.match(/\/api\/categories\/id\/\d+$/) && connection.request.method === RequestMethod.Get) {
                if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    let urlParts = connection.request.url.split('/');
                    let categoryId = parseInt(urlParts[urlParts.length - 1]);

                    console.log('reading category:');
                    console.log(JSON.stringify(categoryId));

                    let category = categoryRepository.getCategoryById(categoryId);

                    console.log('categories');
                    console.log(JSON.stringify(categoryRepository.readCategories()));

                    connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: category })));
                }
                else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
                }
                return;
            }

            if (connection.request.url.endsWith('/api/categories') && connection.request.method === RequestMethod.Get) {
                // check for fake auth token in header and return test users if valid, this security is implemented server side
                // in a real application
                if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {

                    console.log('ALL CATEGORIES API');
                    let categories=categoryRepository.readCategories();
                    console.log(JSON.stringify(categories));

                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200, body: categories })
                    ));
                } else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 401 })
                    ));
                }
            }

            if (connection.request.url.match(/\/api\/tasksWebNotification\/username\/\w+$/) && connection.request.method === RequestMethod.Get) {
                if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    let urlParts = connection.request.url.split('/');
                    let username = urlParts[urlParts.length - 1];
                    let tasks=taskRepository.getTasksWebNotificationByUsername(username);
                    let tasksString=JSON.stringify(tasks);

                    console.log('GET TASKS WEB NOTIFICATION BY USERNAME BACKEND');
                    console.log(tasksString);

                    if(tasks==null){
                        connection.mockRespond(new Response(new ResponseOptions({ status: 404 })));
                    }
                    else{
                        connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: tasks })));
                    }
                }
                else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
                }
                return;
            }
        }, 500);
    });

    return new Http(backend, options);
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: Http,
    useFactory: fakeBackendFactory,
    deps: [MockBackend, BaseRequestOptions, TaskRepository, CategoryRepository, UserRepository]
};