﻿import { Component, OnInit } from '@angular/core';

import { UserService } from '../_services/index';
import { User } from "../_models/user";
import {TaskService} from "../_services/task.service";
import {Task} from "../_models/task";
import {AuthenticationService} from "../_services/authentication.service";

@Component({
    moduleId: module.id,
    templateUrl: 'adminHome.component.html'
})
export class AdminHomeComponent implements OnInit {
    users: User[] = [];
    getUserApiTest: User;
    getTaskByUsernameApiTest: Task[];

    constructor(
        private authenticationService:AuthenticationService,
        private taskService: TaskService,
        private userService: UserService) { }

    ngOnInit() {
        //get users from secure api end point
        this.taskService.getTasksByUsername('user')
            .subscribe(response => {
                this.getTaskByUsernameApiTest=response;
            });

        this.userService
            .getUsers()
                .subscribe(users => {
                    this.users = users;
                });

        this.userService
            .getUser('admin')
                .subscribe(user=>{
                    this.getUserApiTest=user;
                });


        // this.userService.getUsers()
        //     .subscribe(users => {
        //         this.getUserApiTest = users;
        //     });


        // this.userService.getUser('admin')
        //     .subscribe(user=>{
        //         this.getUserApiTest=user;
        //     })
    }
    logout(){
        this.authenticationService.logout();
    }
}