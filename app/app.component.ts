﻿import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {UserRepository} from "./_repositories/userRepository";
import {TaskRepository} from "./_repositories/taskRepository";
import {AuthenticationService} from "./_services/authentication.service";

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html'
})

export class AppComponent {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
    ) {
        // console.log('UPDATE TASKS');
        // //TaskRepository.initialize();
        //
        // //console.log(JSON.stringify(TaskRepository.getTasksByUsername('user')));
        //
        // console.log('UPDATE USERS');
        // UserRepository.update();

        console.log(router.url);
    }

    logout(){
        this.authenticationService.logout();
    }
}