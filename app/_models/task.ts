import {User} from "./user";
import {TaskStatus} from "./taskStatus";
export class Task{
    id:number;
    categoryId:number;
    username:string;
    name:string;
    startDate:Date;
    duration:number;
    status:TaskStatus;
    description:string;
    webNotificationDate:Date;
    eMailNotificationDate:Date;

    constructor(id: number, categoryId: number, username: string, name: string, startDate: Date, duration: number, status: TaskStatus, description: string, webNotificationDate: Date, eMailNotificationDate: Date) {
        this.id = id;
        this.categoryId = categoryId;
        this.username = username;
        this.name = name;
        this.startDate = startDate;
        this.duration = duration;
        this.status = status;
        this.description = description;
        this.webNotificationDate=webNotificationDate;
        this.eMailNotificationDate=eMailNotificationDate;
    }

    static convertFromResponseToTask(params:any){
        let startDateString=params.startDate;
        let startDateUtc=new Date(startDateString);
        let startDate=new Date(startDateUtc);
        startDate.setHours(startDateUtc.getHours()+2);

        let webNotificationString=params.webNotificationDate;
        let webNotificationDateUtc=new Date(webNotificationString);
        let webNotificationDate=new Date(webNotificationDateUtc);
        webNotificationDate.setHours(webNotificationDateUtc.getHours()+2);

        let eMailNotificationString=params.eMailNotificationDate;
        let eMailNotificationDateUtc=new Date(eMailNotificationString);
        let eMailNotificationDate=new Date(eMailNotificationDateUtc);
        eMailNotificationDate.setHours(eMailNotificationDateUtc.getHours()+2);


        let task=new Task(
            params.id,
            params.categoryId,
            params.username,
            params.name,
            startDate,
            params.duration,
            params.status,
            params.description,
            webNotificationDate,
            eMailNotificationDate
        );

        return task;
    }

    static convertFromTasksToResponse(tasks: Task[]){
        let response=[];
        for(let task of tasks){
            response.push(this.convertFromTaskToResponse(task));
        }
        return response;
    }

    static convertFromTaskToResponse(task : Task){
        // return {
        //     id:task.id,
        //     categoryId:task.categoryId,
        //     username:task.username,
        //     name:task.name,
        //     startDate:task.startDate.toUTCString(),
        //     duration:task.duration,
        //     status:task.status,
        //     description:task.description,
        //     webNotificationDate:task.webNotificationDate.toUTCString(),
        //     eMailNotificationDate:task.eMailNotificationDate.toUTCString()
        // }
        return {
            id:task.id,
            categoryId:task.categoryId,
            username:task.username,
            name:task.name,
            startDate:task.startDate.toString(),
            duration:task.duration,
            status:task.status,
            description:task.description,
            webNotificationDate:task.webNotificationDate.toString(),
            eMailNotificationDate:task.eMailNotificationDate.toString()
        }
    }
}