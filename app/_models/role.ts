export enum Role {
    Admin,
    User,
    Anonymous
}

export namespace Role {
    export function fromStringToEnum(role:string) {
        switch (role){
            case 'administrator':
                return Role.Admin;
            case 'użytkownik':
                return Role.User;
        }
    }

    export function fromEnumToString(role:Role) {
        switch (role){
            case Role.Admin:
                return 'administrator';
            case Role.User:
                return 'użytkownik';
        }
    }

    export function toStringArray(){
        return [
            'administrator',
            'użytkownik'
        ]
    }
}
