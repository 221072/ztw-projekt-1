import {TaskBoardDay} from "./taskBoard";
import {Params} from "@angular/router";
export class TaskBoardDate{
    dayName:string;
    dayNumber:number;
    month:string;
    year:number;

    date:Date;

    constructor(dayName: string, dayNumber: number, month: string, year: number, date:Date) {
        this.dayName = dayName;
        this.dayNumber = dayNumber;
        this.month = month;
        this.year = year;
        this.date=date;
    }

    // static initialize(params:Params){
    //     // this.firstname = params["firstname"];
    //     // this.lastname = params["lastname"];
    //     return new TaskBoardDate(
    //         params['dayName'],
    //         params['dayNumber'],
    //         params['month'],
    //         params['year'],
    //
    //     )
    // }
}

export enum PolishMonth{
    Styczeń,
    Luty,
    Marzec,
    Kwiecień,
    Maj,
    Czerwiec,
    Lipiec,
    Sierpień,
    Wrzesień,
    Październik,
    Listopad,
    Grudzień
}

export namespace PolishMonth{
    // export function fromStringToEnum(role:string) {
    //     switch (role){
    //         case 'administrator':
    //             return Role.Admin;
    //         case 'użytkownik':
    //             return Role.User;
    //     }
    // }
    //
    // export function fromEnumToString(role:Role) {
    //     switch (role){
    //         case Role.Admin:
    //             return 'administrator';
    //         case Role.User:
    //             return 'użytkownik';
    //     }
    // }

    export function toStringArray(){
        var monthArray=[];
        for(var i=0;i<12;i++){
            monthArray.push(PolishMonth[i].toString());
        }

        // return [
        //     'Styczeń',
        //     'Luty',
        //     'Marzec',
        //     'Kwiecień',
        //     'Maj',
        //     'Czerwiec',
        //     'Lipiec',
        //     'Sierpień',
        //     'Wrzesień',
        //     'Październik',
        //     'Listopad',
        //     'Grudzień'
        // ];
    }
}

export enum PolishDay{
    Niedziela,
    Poniedziałek,
    Wtorek,
    Środa,
    Czwartek,
    Piątek,
    Sobota
}

export namespace PolishDay{
    export function toStringArray(){
        var dayArray=[];
        for(var i=0;i<7;i++){
            dayArray.push(PolishDay[i].toString());
        }
    }
}