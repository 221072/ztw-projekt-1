import {Task} from "./task";
import {PolishDay, PolishMonth, TaskBoardDate} from "./taskBoardDate";

export class TaskBoardDay{
    date:TaskBoardDate;
    tasks:Task[];

    // constructor(day: TaskBoardDate, tasks: Task[]) {
    //     this.date = day;
    //     this.tasks = tasks;
    // }


    constructor() {
        this.tasks=[];
    }

    addTask(task:Task){
        //let taskStartDate=task.startDate;

        console.log('TaskBoardDay ADD TASK');
        console.log(JSON.stringify(task));

        let taskStartDate=new Date(task.startDate);

        let day=taskStartDate.getDay();
        let month=taskStartDate.getMonth();
        let year=taskStartDate.getFullYear();

        console.log(JSON.stringify(this.date));

        console.log('POLISH DAY: '+day+': '+PolishDay[day]);
        console.log('POLISH MONTH: '+month+': '+PolishMonth[month]);

        if (
            this.date.dayName==PolishDay[day] &&
            this.date.month==PolishMonth[month] &&
            this.date.year==year
        ){
            console.log('PUSHED');
            this.tasks.push(task);
            return true;
        }
        return false;
    }
}

export class TaskBoard{
    boardDays:TaskBoardDay[];

    // constructor(boardDays: TaskBoardDay[]) {
    //     this.boardDays = boardDays;
    // }


    constructor() {
        this.boardDays=[];
    }

    addDay(taskBoardDay:TaskBoardDay){
        this.boardDays.push(taskBoardDay);
    }

    addTasks(tasks:Task[]){
        console.log('ADD TASK: ');
        //console.log(JSON.stringify(tasks));
        for(let task of tasks){
            let dateString=task.startDate;
            let date=new Date(dateString);
            task.startDate=date;
            console.log('CURRENT TASK DATE: '+JSON.stringify(task.startDate));
            for(let boardDay of this.boardDays){
                console.log('CURRENT BOARD DAY: '+JSON.stringify(boardDay.date));
                if(boardDay.addTask(task)){
                    console.log('FOUND');
                    console.log(JSON.stringify(boardDay));
                    console.log(JSON.stringify(task));
                    break;
                }
            }
            console.log('TASK BOARD'+JSON.stringify(this.boardDays));
        }
        console.log('TASK BOARD END'+JSON.stringify(this.boardDays));
    }
}