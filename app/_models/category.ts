import {Color} from "./color";
export class Category{
    categoryId: number;
    name: string;
    description: string;
    color: Color;

    constructor(categoryId: number, name: string, description: string, color: Color) {
        this.categoryId = categoryId;
        this.name = name;
        this.description = description;
        this.color = color;
    }

    static convertFromResponseToCategory(params:any){
        let category=new Category(params.categoryId,params.name, params.description,params.color);
        return category;
    }

    static convertFromCategoryToResponse(category : Category){
        return {
            categoryId:category.categoryId,
            name:category.name,
            description:category.description,
            color:category.color
        };
    }
}