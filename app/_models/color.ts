export enum Color {
    Red,
    Green,
    Blue,
    White,
    Grey,
    Silver
}

export namespace Color {
    export function fromStringToEnum(color:string) {
        switch (color){
            case 'czerwony':
                return Color.Red;
            case 'zielony':
                return Color.Green;
            case 'niebieski':
                return Color.Blue;
            case 'biały':
                return Color.White;
            case 'szary':
                return Color.Grey;
            case 'srebrny':
                return Color.Silver;
        }
    }

    export function fromEnumToString(color:Color) {
        switch (color){
            case Color.Red:
                return 'czerwony';
            case Color.Green:
                return 'zielony';
            case Color.Blue:
                return 'niebieski';
            case Color.White:
                return 'biały';
            case Color.Grey:
                return 'szary';
            case Color.Silver:
                return 'srebrny';
        }
    }

    export function toStringArray(){
        return [
            'czerwony',
            'zielony',
            'niebieski',
            'biały',
            'szary',
            'srebrny'
        ]
    }
}
