﻿import {Role} from "./role";
export class User {
    username: string;
    password: string;
    eMail: string;
    role: Role;

    constructor(username: string, password: string, eMail: string, role: Role) {
        this.username = username;
        this.password = password;
        this.eMail = eMail;
        this.role = role;
    }

    public isValidUser(username:string, password:string){
        return this.username === username && this.password === password
    }
}