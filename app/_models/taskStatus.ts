export enum TaskStatus {
    New,
    InProgress,
    Complete
}

export namespace TaskStatus {
    export function fromStringToEnum(taskStatus:string) {
        switch (taskStatus){
            case 'nowy':
                return TaskStatus.New;
            case 'w trakcie wykonywania':
                return TaskStatus.InProgress;
            case 'ukończony':
                return TaskStatus.Complete;
        }
    }

    export function fromEnumToString(taskStatus:TaskStatus) {
        switch (taskStatus){
            case TaskStatus.New:
                return 'nowy';
            case TaskStatus.InProgress:
                return 'w trakcie wykonywania';
            case TaskStatus.Complete:
                return 'ukończony'
        }
    }

    export function toStringArray(){
        return [
            'nowy',
            'w trakcie wykonywania',
            'ukończony'
        ]
    }
}
