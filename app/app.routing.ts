﻿import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/index';
import { AdminHomeComponent } from './adminHome/index';
import { AdminAuthGuard } from './_guards/index';
import {AccountManagementComponent} from "./accountManagement/accountManagement.component";
import {AccountFormComponent} from "./accountForm/accountForm.component";
import {UserAuthGuard} from "./_guards/user.auth.guard";
import {UserHomeComponent} from "./userHome/userHome.component";
import {CategoryManagementComponent} from "./categoryManagement/categoryManagement.component";
import {EditTaskComponent} from "./editTask/editTask.component";
import {AddTaskComponent} from "./addTask/addTask.component";
import {EditCategoryComponent} from "./editCategory/editCategory.component";
import {AddCategoryComponent} from "./addCategory/addCategory.component";
import {HomeComponent} from "./home/home.component";

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },

    { path: 'adminHome', component: AdminHomeComponent, canActivate: [AdminAuthGuard] },
    { path: 'userHome', component: UserHomeComponent, canActivate: [UserAuthGuard] },

    { path: 'accountManagement', component: AccountManagementComponent, canActivate: [AdminAuthGuard] },

    { path: 'accountForm/:username', component: AccountFormComponent, canActivate: [AdminAuthGuard] },
    { path: 'accountForm', component: AccountFormComponent },

    { path: 'editTask/:taskId', component: EditTaskComponent, canActivate: [UserAuthGuard] },
    { path: 'addTask/:date', component: AddTaskComponent, canActivate: [UserAuthGuard] },

    { path: 'categoryManagement', component: CategoryManagementComponent, canActivate: [AdminAuthGuard] },
    { path: 'editCategory/:categoryId', component: EditCategoryComponent, canActivate: [AdminAuthGuard] },
    { path: 'addCategory', component: AddCategoryComponent, canActivate: [AdminAuthGuard] },
    { path: '**', component: HomeComponent },

    // otherwise redirect to adminHome
    // { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);